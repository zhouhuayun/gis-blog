The StaMPS-Workflow has been updated. This link is deprecated.

Please refer to [StaMPS Workflow](../StaMPS/2_stamps_workflow.md) instead.
