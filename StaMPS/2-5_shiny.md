# Table of Contents

* [Overview](./2_stamps_workflow.md)
* [Region of Interest](./2-1_roi.md)
* [Get SENTINEL-1 data](./2-2_get_data.md)
* [snap2stamps](./2-3_snap2stamps.md)
* [StaMPS](./2-4_StaMPS-steps.md)
* [Shiny](#stamps-visualizer)

# StaMPS Visualizer
[StaMPS Vizualizer](https://github.com/thho/StaMPS_Visualizer) is a Shiny App in R
for interactively visualizing StaMPS results. Discussion of the app can be found
at the [ESA Step Thread](https://forum.step.esa.int/t/stamps-visualizer-snap-stamps-workflow/9613).

Export csv-file for visualization in StaMPS Vizualizer:
```matlab
corr_method = 'a_gacos; ' % 'a_linear' / 'a_erai'
ps_plot('v-dao', corr_method, 'ts');
% after the plot has appeared magically, set radius and location by clicking into the plot
load parms.mat;
ps_plot('v-dao', corr_method, -1);
load ps_plot_v-dao.mat;
lon2_str = cellstr(num2str(lon2));
lat2_str = cellstr(num2str(lat2));
lonlat2_str = strcat(lon2_str, lat2_str);

lonlat_str = strcat(cellstr(num2str(lonlat(:,1))), cellstr(num2str(lonlat(:,2))));
ind = ismember(lonlat_str, lonlat2_str);

disp = ph_disp(ind);
disp_ts = ph_mm(ind,:);
export_res = [lon2 lat2 disp disp_ts];

metarow = [ref_centre_lonlat NaN transpose(day)-1];
k = 0;
export_res = [export_res(1:k,:); metarow; export_res(k+1:end,:)];
export_res = table(export_res);
writetable(export_res,'stamps_tsexport.csv')
```
