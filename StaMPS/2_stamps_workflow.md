# Table of Contents

* [Introduction](#introductory-comments)
* [StaMPS setup](./1_stamps_setup.md)
* [Region of Interest](./2-1_roi.md)
* [Get SENTINEL-1 data](./2-2_get_data.md)
* [snap2stamps](./2-3_snap2stamps.md)
* [StaMPS](./2-4_StaMPS-steps.md)
* [StaMPS Visualizer](./2-5_shiny.md)
* [LOS to vertical](./3_stamps_los2vertical.md)

# Introductory Comments

This document is intended to...

1. provide guidance on how to setup StaMPS for the first time, 
2. outline a general workflow of SNAP-StaMPS PS-analysis,
3. provide updates on default parameter settings to use for selected applications,
4. provide assistance in visualizing the results.

This document is largely based on the excellent
[*SNAP-StaMPS Workflow*](https://forum.step.esa.int/t/how-to-prepare-sentinel-1-images-stack-for-psi-sbas-in-snap-5/4981/514)
compiled by Thorsten Höser as well as discussions and presented workflows from
[MAINSAR](https://groups.google.com/forum/#!topic/mainsar/38KZ2-6nbrI),
step forum on [snap-stamps workflow](https://forum.step.esa.int/t/workflow-between-snap-and-stamps/3211/9)
and the [stamps-tool](https://forum.step.esa.int/t/about-the-stamps-category/8140)
as well as [Ask Ubuntu](https://askubuntu.com). Thanks to all members and developers
contributing ideas, solving problems and helping to make this workflow as
smooth as possible.

Please note that this summary has been written and tested on a certain machine
using a specific user. Even though it has been tried to generalize code as much
as possible, certain paths may have to be adjusted accordingly.

The used OS is Ubuntu 18.04 LTS.

